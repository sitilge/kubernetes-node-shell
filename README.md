# kubernetes-node-shell

A simple pod that runs in the host node namespace. The pod will be terminated in 100 minutes. To assign the pod to a certain node, use `nodeSelector`.
